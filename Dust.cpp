/*
 * Defines general methods for simulating "dust," i.e. solids particularly
 * in the case of protplanetary disks.
 * 
 * This is the general dust module file, for classes/functions used by all
 * dust implementations and by the dust sub-modules.
 * 
 * Author: Isaac Backus (University of Washington)
 * Created: 4/18/2016
 */

#include "ParallelGravity.h"  // Things work best if this is included 1st
#include "Dust.h"
#include "DustOutputParams.h"

#include "Sph.h"  // Included to use arrayFileExists()

/**
 * @brief AddParams initializes runtime parameters for dust
 * 
 * The parameters to be set are:
 *      dDustGrainDensity - Intrinsic dust grain density in code units (mass
 *          per volume), e.g. for silica
 *      dDustVfrag - Fragmentation velocity for dust grains in collisions
 *          in code units.
 *      dDustAlphaSS - Constant for the Shakura-Sunyaev viscosity parameter
 *          in local grain growth code
 */
void DustParam::AddParams(PRM prm){
    /* Load in dust parameters */

    dDustGrainDensity = 0.0;
    prmAddParam(prm, "dDustGrainDensity", paramDouble, &dDustGrainDensity, 
                sizeof(double), "dustden", 
                "<Intrinsic grain density (code units)> = 0.0");
    dDustVfrag = 0.0;
    prmAddParam(prm, "dDustVfrag", paramDouble, &dDustVfrag, 
                sizeof(double), "dustvfrag", 
                "<fragmentation velocity for dust grains (code units)> = 0.0");
    dDustAlphaSS = 0.0;
    prmAddParam(prm, "dDustAlphaSS", paramDouble, &dDustAlphaSS, 
                sizeof(double), "dustalphass", 
                "<Shakura&Sunyaev viscosity for Vrel model (code units)> = 0.0");
    dConstGrainSize = 0.0;
    prmAddParam(prm, "dConstGrainSize", paramDouble, &dConstGrainSize, 
                sizeof(double), "grainsize", 
                "<Constant dust grain size (code units)> = 0.0");
}
/**
 * @brief DustParam::CheckParams checks runtime parameters for dust and sets
 * params required for dustygas.
 */
void DustParam::CheckParams(PRM prm, parameters &param) {
    /* Turn off things which don't work with dust */
    if (param.dEtauDot > 0.0) {
        ckerr << "WARNING: dEtauDot not implemented for dustygas.  "
                 "Setting dEtauDot = 0." << endl;
        param.dEtauDot = 0.0;
    }
    if (param.bFastGas != 0) {
        ckerr << "WARNING: fast gas is buggy for dustygas. "
                 "Consider setting bFastGas = 0" << endl;
    }
}


/**
 * @brief Main::writeDust Handles writing out dust arrays
 * @param achFile Base filename (excluding extension) 
 * @param dOutTime Simulation time
 */
void Main::writeDust(std::string achFile, double dOutTime) {
    dustonefluid::write(this, param, achFile, dOutTime);
}

/**
 * @brief Main::initDust Initializes dustygas at the beginning of a simulation
 */
void Main::initDust() {
    ckout << "Initializing dust... ";
    ckout << endl;
    // Deal with default params
    param.dustparams.CheckParams(prm, param);
    // Initialize sub-modules
    dustonefluid::init(param, nTotalParticles, dTime);
}
