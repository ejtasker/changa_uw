/*
 * Declare general methods for the one-fluid dust submodule, used for simulating
 * dust (solids) using a single-fluid treatment.
 * 
 * Author: Isaac Backus (University of Washington)
 * Created: 10/12/17
 */
#ifndef DUSTONEFLUID_H
#define DUSTONEFLUID_H
// Forward declarations
class Main;
struct parameters;

#include "Dust.h"
#include "GravityParticle.h"
#include "smoothparams.h"

namespace dustonefluid {

// Constants
const double MAXDUSTFRAC = 1.0 - 1e-5;
const double MINDUSTFRAC = 0.0;
const double DUSTFRACEPS = 0.0; // Can be slightly > 0.0 to avoid divide by zero


// Inline functions
inline double grainSizePred(GravityParticle *p, double grainSize) {
#ifdef DUSTGROWTH
    return p->dustGrainSizePred();
#else
    return grainSize;
#endif
}

// Functions

void init(parameters &param, int64_t nTotalParticles, double dTime) ;
void smoothDust(int activeRung, double dTime, parameters &param);

Vector3D<double> dustVelocity(GravityParticle *p);

void scaleViscosity(GravityParticle *p, GravityParticle *q, double &visc);

void localSPHterms(GravityParticle *p, parameters &param, double gamma);

void limitDustFrac(double &dustFrac);

void openKick(GravityParticle *p, double dt);

void drift(GravityParticle *p, double dt);

void closeKick(GravityParticle *p, double dt);

double gasFracNorm(GravityParticle *p, double quantity);

void updateAcc(GravityParticle *p, const Vector3D<double> &acc3d);

double getDustFrac(GravityParticle *p);

void setDustFrac(GravityParticle *p, double dustFrac);

void sinkDust(GravityParticle *p, GravityParticle *q);

void write(Main *main, parameters &param, std::string achFile, 
                   double dOutTime);

class SmoothPar : public SmoothParams {
    
    virtual void fcnSmooth(GravityParticle *p, int nSmooth,
               pqSmoothNode *nnList);
    virtual int isSmoothActive(GravityParticle *p);
    virtual void initTreeParticle(GravityParticle *p) {}
    virtual void postTreeParticle(GravityParticle *p) {}
    virtual void initSmoothParticle(GravityParticle *p);
    virtual void initSmoothCache(GravityParticle *p);
    virtual void combSmoothCache(GravityParticle *p1,
                 ExternalSmoothParticle *p2);
    int dTime;
    double dDustGrainDensity;
public:
    // Constructors
    SmoothPar() {}
    SmoothPar(int _iType, int _activeRung, double _dTime, 
                     parameters *param);
    PUPable_decl(SmoothPar);
    SmoothPar(CkMigrateMessage *m) : SmoothParams(m) {}
    // PUP
    virtual void pup(PUP::er &p) {
        // Call base class
        SmoothParams::pup(p);
        // Additional parameters to pup
        p|dTime;
        p|dDustGrainDensity;
    }
};

/**
 * @brief The pressureSmooth class defines functions used in 
 * PressureSmoothParams to calculate dust terms in the SPH 
 * equations.
 */
class pressureSmooth {
public:
    static void initSmoothParticle(GravityParticle *p);
    static void initSmoothCache(GravityParticle *p);
    static void combSmoothCache(GravityParticle *p1, ExternalSmoothParticle *p2);
    pressureSmooth(GravityParticle *self) {}
};

} // namespace dustonefluid
#endif // DUSTONEFLUID_H
