/*
 * Declare general methods for the one-fluid grain growth submodule.
 * 
 * This is a sub-module of the DustOneFluid sub-module.
 * 
 * Author: Isaac Backus (University of Washington)
 * Created: 10/12/17
 */
#ifndef DUSTONEFLUIDGRAINS_H
#define DUSTONEFLUIDGRAINS_H

#include "Dust.h"

// Forward declaration needed
class DustParam;

// Constants
const double DUSTFRACLOSSEPS = 1e-12;
const double ROSSBYNUMBER = 3; // Rossby number for grain growth algorithm

namespace onefluidgrains {
void updateReceiver(GravityParticle *receiver, GravityParticle *donor, 
                    double dustFracDotVal);
void updateDonor(GravityParticle *receiver, GravityParticle *donor, 
                 double dustFracDotVal);
void initSmoothParticle(GravityParticle *p);
void initSmoothCache(GravityParticle *p);
void combSmoothCache(GravityParticle *p1, ExternalSmoothParticle *p2);
void init(parameters &param, int64_t nTotalParticles, double dTime);
void localGrainGrowth(GravityParticle*p, DustParam &dustparam, double gamma);
void openKick(GravityParticle *p, double dt);
void drift(GravityParticle *p, double dt);
void closeKick(GravityParticle *p, double dt);
void write(Main *main, parameters &param, std::string achFile, double dOutTime);

} // namespace onefluidgrains

#endif // DUSTONEFLUIDGRAINS_H
