/*
 * Defines general methods for the one-fluid dust submodule, used for simulating
 * dust (solids) using a single-fluid treatment.
 * 
 * By convention, everything declared in this sub-module (i.e. in the header
 * corresponding to this file) should be under this sub-module's namespace.
 * Everything declared outside this submodule and implemented here should be
 * outside this submodule's name-space.  This is both to protect names and to
 * make function calls more transparent when called from other parts of 
 * ChaNGa.
 * 
 * Also by convention, this module is turned off by compiling 
 * DustOneFluidOff.cpp (instead of this file) which contains empty
 * (or properly modified) function definitions for functions called by the
 * rest of the code.
 * 
 * Authors: Isaac Backus (University of Washington)
 *          Josef Rucska (McMaster University)
 * Created: 10/12/17
 */
#include "ParallelGravity.h" // Things tend to work better if included 1st
#include "DustOneFluid.h"
#include "DustOneFluidGrains.h"
#include "DustOutputParams.h"
#include "smooth.h"
#include "Sph.h" // Needed only for arrayFileExists()

// USED FOR SANITY CHECK
#include <iostream>
#include <string>

namespace dustonefluid {

/**
 * @brief dustVelocity calculates the dust velocity in the terminal velocity
 * approximation.
 * 
 * Dust velocity is given by:
 *      v_dust = v_particle + t_stop*(1 - eps)*(a_dust - a_gas)
 * 
 * Gas velocity can be derived as:
 *      v_gas = v_particle + (eps/(1-eps))*(v_particle - v_dust)
 * 
 * where eps is the dust fraction
 * @param p gas particle
 * @return 
 */
Vector3D<double> dustVelocity(GravityParticle *p) {
    Vector3D<double> dustVel;
    dustVel = p->tStop() * p->gasDustDiffAcc();
    dustVel += p->velocity;
    return dustVel;
}

/**
 * @brief sinkDust accretes dust fraction onto a sink particle.  This should
 * be called before accreting mass onto the particle
 * @param p sink particle
 * @param q neighbor
 */
void sinkDust(GravityParticle *p, GravityParticle *q) {
    double pDustFrac = getDustFrac(p);
    double qDustFrac = getDustFrac(q);
    pDustFrac = (pDustFrac*p->mass + qDustFrac*q->mass) / (p->mass + q->mass);
    setDustFrac(p, pDustFrac);
}

/**
 * @brief getDustFrac returns a particle's dust fraction, agnostic to the 
 * particle type.
 * @param p particle
 * @return particle dust fraction
 */
double getDustFrac(GravityParticle *p) {
    if (p->isGas()) {
        return p->dustFrac();
    } else if (p->isStar()) {
        return p->starDustFrac();
    } else {
        CkAbort("Dust fraction only available for star and gas particles");
    }
}

/**
 * @brief setDustFrac sets a particle's dust fraction, agnostic to the particle
 * type.
 * @param p
 * @param dustFrac
 */
void setDustFrac(GravityParticle *p, double dustFrac) {
    if (p->isGas()) {
        p->dustFrac() = dustFrac;
    } else if (p->isStar()) {
        p->starDustFrac() = dustFrac;
    } else {
        CkAbort("Dust fraction only available for star and gas particles");
    }
}

/**
 * @brief updateAcc updates the difference in acceleration (specific force)
 * between the dust and gas components during SPH calculation when performing
 * two-loops for dust 2nd derivatives.
 * 
 * This algorithm keeps track of the differential acceleration multiplied 
 * by (1-dustFrac) to avoid some divide-by-zeros.  That is to say:
 *      p->gasDustDiffAcc() = (acc_dust - acc_gas) * (1 - dustFrac)
 * @param p Particle to update
 * @param acc3d SPH acceleration being added to the gas component.
 */
void updateAcc(GravityParticle *p, const Vector3D<double> &acc3d) {
    p->gasDustDiffAcc() -= acc3d;
}

/**
 * @brief scaleViscosity rescales the viscosity terms by (1-dustFrac).
 * @param p 1st interacting particle
 * @param q 2nd (neighbor) interacting particle
 * @param visc Viscosity term (gets updated by this function)
 */
void scaleViscosity(GravityParticle *p, GravityParticle *q, double &visc){
    // Note: it's not clear if we should use average dustFraction here or just
    // p->dustFrac
    visc *= 1.0 - 0.5*(p->dustFracPred() + q->dustFracPred());
}

/**
 * @brief localSPHterms is used in TreePiece::getAdiabaticGasPressure()
 * (defined in Sph.C) to re-scale pressure and to calculate dust stopping time
 * 
 * NOTE: gamma in this function should be 1 for isothermal gas.
 * 
 * @param p SPH particle to update
 * @param grainDensity Intrinisic grain density (in code units)
 * @param Vfrag Fragmentation velocity for dust grains (in code units)
 * @param alphaSS Constant for Shakura-Sunyaev viscosity in local grain growth
 * @param gamma Adiabatic index
 */
void localSPHterms(GravityParticle *p, parameters &param, double gamma) {
    DustParam &dustparam = param.dustparams;
    // Re-scale PoverRho to be pressure/total density
    p->PoverRho2() *= 1 - p->dustFracPred();
    // Calculate dust stopping time in the Epstein regime
    p->tStop() = dustparam.dDustGrainDensity * 
            grainSizePred(p, dustparam.dConstGrainSize) / (p->fDensity*p->c());
    p->tStop() *= sqrt(M_PI*gamma/8.);
    onefluidgrains::localGrainGrowth(p, dustparam, gamma);
#ifdef DUSTDIFFUSIONTEST
    // For the diffusion test, this allows us to use a fixed stopping time
    p->tStop() = 0.1;
#endif //DUSTDIFFUSIONTEST
}

/**
 * @brief gasFracNorm normalizes quantity by 1-dustFrac (if dustygas is enabled.)
 */
double gasFracNorm(GravityParticle *p, double quantity) {
    quantity /= 1. - p->dustFracPred() + DUSTFRACEPS;
    if (quantity != quantity) {
        CkError("NaN encountered in gasFracNorm");
        CkAssert(quantity == quantity);
    }
    return quantity;
}

/**
 * @brief limitDustFrac forces the dust fraction to be within certain bounds
 * (zero and one, by default)
 */
void limitDustFrac(double &dustFrac) {
    if (dustFrac < MINDUSTFRAC) {
        dustFrac = MINDUSTFRAC;
    } else if(dustFrac > MAXDUSTFRAC) {
        dustFrac = MAXDUSTFRAC;
    } else if(dustFrac != dustFrac) {
        CkError("NaN encountered in dustFrac");
        CkAssert(dustFrac == dustFrac);
    }
}

/* *********************************************
 * pressureSmooth
 * *********************************************/
/**
 * @brief pressureSmooth::initSmoothParticle is used by 
 * PressureSmoothParams::initSmoothParticle() for dust
 */
void pressureSmooth::initSmoothParticle(GravityParticle *p){
    p->gasDustDiffAcc() = 0.0;
}
/**
 * @brief pressureSmooth::initSmoothCache is used by 
 * PressureSmoothParams::initSmoothCache() for dust
 */
void pressureSmooth::initSmoothCache(GravityParticle *p) {
    p->gasDustDiffAcc() = 0.0;
}
/**
 * @brief pressureSmooth::combSmoothCache is used by
 * PressureSmoothParams::combSmoothCache() for dust
 */
void pressureSmooth::combSmoothCache(GravityParticle *p1, 
                                         ExternalSmoothParticle *p2) {
    p1->gasDustDiffAcc() += p2->gasDustDiffAcc;
}

/* *********************************************
 * Kick-drift functions
 * *********************************************/
/**
 * @brief openKick is called by TreePiece::kick() when opening a kick during
 * the kick-drift integration the dust fraction.
 * @param p Pointer to the SPH particle to open the integration step for.
 * @param dt Time step size (in code units)
 */
void openKick(GravityParticle *p, double dt) {
    onefluidgrains::openKick(p, dt);
    p->dustFracPred() = p->dustFrac();
    p->dustFrac() += p->dustFracDot() * dt;
    // Handle the gas of dust fraction outside of 0 to 1
    limitDustFrac(p->dustFrac());
}

/**
 * @brief drift is called by TreePiece::drift() to drift the dust fraction
 * during the kick-drift integration of the dust fraction
 * @param p Pointer to the SPH particle to drift
 * @param dt Time step size (in code units)
 */
void drift(GravityParticle *p, double dt) {
    onefluidgrains::drift(p, dt);
    p->dustFracPred() += p->dustFracDot() * dt;
    // Handle the gas of dust fraction outside of 0 to 1
    limitDustFrac(p->dustFracPred());
}

/**
 * @brief closeKick is called by TreePiece::kick() when closing the kick
 * during the kick-drift integration of the dust fraction
 * @param p Pointer to the SPH particle to close the kick on
 * @param dt Time step size (in code units)
 */
void closeKick(GravityParticle *p, double dt) {
    onefluidgrains::closeKick(p, dt);
    p->dustFrac() += p->dustFracDot() * dt;
    // Handle the gas of dust fraction outside of 0 to 1
    limitDustFrac(p->dustFrac());
    p->dustFracPred() = p->dustFrac();
}

/* *********************************************
 * SmoothPar
 * *********************************************/
/**
 * @brief SmoothPar::SmoothPar constructor. SmoothPar 
 * class handles an extra SPH loop over neighbors for the dust fraction equation
 * @param _iType Particle type
 * @param _activeRung Which rung is active
 * @param _dTime Simulation time in code units
 * @param param Runtime parameters struct
 */
SmoothPar::SmoothPar(int _iType, int _activeRung, double _dTime, 
                                   parameters *param) {
    iType = _iType;
    activeRung = _activeRung;
    dTime = _dTime;

    dDustGrainDensity = param->dustparams.dDustGrainDensity;
}

/**
 * @brief SmoothPar::fcnSmooth Performs the dust fraction equation 
 * loop over neighbors. (see eq. 26 in Price & Laibe 2015)
 * 
 * Forces are calculated using an alternative to the gather-scatter approach.
 * The interaction between particles a and b can be calculated once and only
 * once by looping over neighbors and calculating forces when the smoothing
 * length of the self particle is bigger than the smoothing length of the
 * neighbor particle.  This will fail in the edge case of equal smoothing
 * lengths.
 * 
 * See https://github.com/ibackus/changa_uw/wiki/ChaNGa-gather-scatter.pdf
 * for an explanation
 * 
 * This uses the general SmoothParams framework
 */
void SmoothPar::fcnSmooth(GravityParticle *p, int nSmooth, 
                            pqSmoothNode *nnList) {
    double ih2p;
    double hp;
    double kernelDerivPrefactorp;
    Vector3D<double> scaledForcep;
    Vector3D<double> gradWp;
    // Initial self specific calculations
    hp = 0.5 * p->fBall;
    ih2p = 1.0/(hp*hp);
    kernelDerivPrefactorp = ih2p*ih2p*ih2p*hp;
    scaledForcep = p->gasDustDiffAcc() / p->fDensity * p->tStop();
    // Loop over neighbors
    for (int i = 0; i < nSmooth; ++i) {
        double hq;
        double ih2q;
        double kernelDerivPrefactorq;
        Vector3D<double> scaledForceq;
        double kpf;
        double dustFracDotVal;
        GravityParticle *q;
        double r2p, r2q;
        double advectionTerm;
        Vector3D<double> gradWq;
        
        q = nnList[i].p;
        // The logic here is tricky -- this uses the alternative gather-scatter
        // approach.  This condition ensures forces are not double calculated
        if (q->fBall > p->fBall) 
            continue;
        kpf = (q->fBall == p->fBall ? (0.5/M_PI) : (1.0/M_PI) );
        // (dx/h)^2
        r2p = nnList[i].fKey * ih2p;
        gradWp = kpf * kernelDerivPrefactorp * DKERNEL(r2p, ih2p) *
            nnList[i].dx;
        hq = 0.5 * q->fBall;
        ih2q = 1.0/(hq*hq);
        kernelDerivPrefactorq = ih2q*ih2q*ih2q*hq;
        scaledForceq = q->gasDustDiffAcc() / q->fDensity * q->tStop();
        r2q = nnList[i].fKey * ih2q;
        if (r2q > 4)
            kpf = 0;
        gradWq = -kpf * kernelDerivPrefactorq * DKERNEL(r2q, ih2q) * 
            nnList[i].dx;
        advectionTerm = dot(scaledForceq, gradWq) - dot(scaledForcep, gradWp);
        // Perform up-winding.  Dust moves from the donor to the receiver
        GravityParticle *receiver, *donor;
        if (advectionTerm > 0) {
            // p is getting dust from q
            receiver = p;
            donor = q;
        } else {
            // q is getting dust from p
            receiver = q;
            donor = p;
        }
        dustFracDotVal = donor->dustFracPred() * abs(advectionTerm);
        if (receiver->rung >= activeRung){
            receiver->dustFracDot() += donor->mass * dustFracDotVal;
            onefluidgrains::updateReceiver(receiver, donor, dustFracDotVal);
        }
        if( donor->rung >= activeRung){
            donor->dustFracDot() -= receiver->mass * dustFracDotVal;
            onefluidgrains::updateDonor(receiver, donor, dustFracDotVal);
        } 
    }
}

/**
 * @brief smoothDust performs the SPH sums (loops over neighbors) for time
 * evolving the dust fraction in the one-fluid dust model.
 * @param activeRung current active rung
 * @param dTime Simulation time (code units)
 * @param param Parameters struct
 */
void smoothDust(int activeRung, double dTime, parameters &param){
    // Perform loop over particles for the dust fraction equation
    SmoothPar pDust(TYPE_GAS, activeRung, dTime, &param);
    treeProxy.startReSmooth(&pDust, CkCallbackResumeThread());
}

/**
 * @brief SmoothPar::isSmoothActive Test if a particle is active
 */
int SmoothPar::isSmoothActive(GravityParticle *p) {
    return (TYPETest(p, TYPE_NbrOfACTIVE));
}

/**
 * @brief SmoothPar::initSmoothParticle Initialization when starting
 * dust smooth operations
 * @param p
 */
void SmoothPar::initSmoothParticle(GravityParticle *p) {
    if (p->rung >= activeRung) {
        p->dustFracDot() = 0.0;
        onefluidgrains::initSmoothParticle(p);
    }
}

/**
 * @brief SmoothPar::initSmoothCache Initialization for copying 
 * particle data across processors during dust smooth operation
 * @param p
 */
void SmoothPar::initSmoothCache(GravityParticle *p) {
    if (p->rung >= activeRung) {
        p->dustFracDot() = 0.0;
        p->PdV() = 0.0;
        onefluidgrains::initSmoothCache(p);
    }
}

/**
 * @brief SmoothPar::combSmoothCache Combine particle data across
 * processors after performing dust smooth operations
 */
void SmoothPar::combSmoothCache(GravityParticle *p1,
             ExternalSmoothParticle *p2) {
    if (p1->rung >= activeRung) {
        p1->dustFracDot() += p2->dustFracDot;
        p1->PdV() += p2->PdV;
        onefluidgrains::combSmoothCache(p1, p2);
    }
}

/* *********************************************
 * Dust I/O
 * *********************************************/
void init(parameters &param, int64_t nTotalParticles, double dTime) {
    string dustfilename;
    // Read in dust fraction
    DustFracOutputParams pDustOut(param.achInFile, 0, dTime);
    dustfilename=std::string(param.achInFile) + "." + pDustOut.sTipsyExt;
    if(arrayFileExists(dustfilename, nTotalParticles)){
        ckout << "\nLoading dust fraction: " << dustfilename.c_str() << endl;
        treeProxy.readTipsyArray(pDustOut, CkCallbackResumeThread());
    }
    // Read in dust fraction time derivative
    DustFracDotOutputParams pDustFracDotOut(param.achInFile, 0, dTime);
    dustfilename=std::string(param.achInFile) + "." + pDustFracDotOut.sTipsyExt;
    if(arrayFileExists(dustfilename, nTotalParticles)){
        ckout << "\nLoading auxiliary dust array " << dustfilename.c_str() << 
            endl;
        treeProxy.readTipsyArray(pDustFracDotOut, CkCallbackResumeThread());
    }
    // Initialize sub-modules
    onefluidgrains::init(param, nTotalParticles, dTime);
}

/**
 * @brief write writes arrays to disk associated with One-fluid dust/ch
 * @param main Pointer to the simulation Main instance
 * @param param Reference to runtime parameters
 * @param achFile Out-file prefix
 * @param dOutTime Current simulation output time
 */
void write(Main *main, parameters &param, std::string achFile, 
                   double dOutTime) {
    // Initialize dust Param objects
    writeArray(DustFracOutputParams(achFile, param.iBinaryOut, dOutTime),
               main, param);
    writeArray(DustFracDotOutputParams(achFile, param.iBinaryOut, dOutTime),
               main, param);
    writeArray(DustVelOutputParams(achFile, param.iBinaryOut, dOutTime),
               main, param);
    writeArray(TstopOutputParams(achFile, param.iBinaryOut, dOutTime),
               main, param);
    onefluidgrains::write(main, param, achFile, dOutTime);
}

} // namespace dustonefluid

/* *********************************************
 * Array I/O class implementations
 * *********************************************/
/**
 * @brief DustVelOutputParams::DustVelOutputParams is used for I/O of
 * a dust array (dustFracDot)
 * 
 * @param _fileName Base filename (excluding extension) for reading/writing
 * @param _iBinaryOut Flag for binary vs ascii files
 * @param _dTime Simulation time
 */
DustVelOutputParams::DustVelOutputParams(std::string _fileName, 
                                             int _iBinaryOut, double _dTime){
    // Set attribute values
    sTipsyExt = "dustVel"; 
    sNChilExt = sTipsyExt;
    bFloat = 1;
    bVector = 1;
    fileName = _fileName; 
    iBinaryOut = _iBinaryOut;
    dTime = _dTime;
    iType = TYPE_GAS;
}
double DustVelOutputParams::dValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
void DustVelOutputParams::setDValue(GravityParticle *p, double val) {
    CkAssert(0);
}
Vector3D<double> DustVelOutputParams::vValue(GravityParticle *p) {
    if (p->isGas()) {
        return dustonefluid::dustVelocity(p);
    } else {
        Vector3D<double> vel = 0.;
        return vel;
    }
        
}
int64_t DustVelOutputParams::iValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
void DustVelOutputParams::setIValue(GravityParticle *p, int64_t iValue) {
    CkAssert(0);
}
/**
 * @brief DustFracOutputParams::DustFracOutputParams is used for I/O of the
 * dust fraction
 * 
 * @param _fileName Base filename (excluding extension) for reading/writing
 * @param _iBinaryOut Flag for binary vs ascii files
 * @param _dTime Simulation time
 */
DustFracOutputParams::DustFracOutputParams(std::string _fileName, 
                                           int _iBinaryOut, double _dTime){
    // Set attribute values
    sTipsyExt = "dustFrac";
    sNChilExt = sTipsyExt;
    bFloat = 1;
    bVector = 0;
    fileName = _fileName; 
    iBinaryOut = _iBinaryOut;
    dTime = _dTime;
    iType = TYPE_GAS | TYPE_STAR;
}
double DustFracOutputParams::dValue(GravityParticle *p) {
    if (p->isGas())
        return p->dustFrac();
    else if (p->isStar())
        return p->starDustFrac();
    else
        return 0.0;
}
void DustFracOutputParams::setDValue(GravityParticle *p, double val) {
    if (p->isGas()) {
        p->dustFrac() = val;
        p->dustFracPred() = val;
    } else if (p->isStar()) {
        p->starDustFrac() = val;
    }
}
Vector3D<double> DustFracOutputParams::vValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
int64_t DustFracOutputParams::iValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
void DustFracOutputParams::setIValue(GravityParticle *p, int64_t iValue) {
    CkAssert(0);
}

/**
 * @brief DustFracDotOutputParams::DustFracDotOutputParams is used for I/O of
 * a dust array (dustFracDot)
 * 
 * @param _fileName Base filename (excluding extension) for reading/writing
 * @param _iBinaryOut Flag for binary vs ascii files
 * @param _dTime Simulation time
 */
DustFracDotOutputParams::DustFracDotOutputParams(std::string _fileName, 
                                             int _iBinaryOut, double _dTime){
    // Set attribute values
    sTipsyExt = "dustFracDot"; 
    sNChilExt = sTipsyExt;
    bFloat = 1;
    bVector = 0;
    fileName = _fileName; 
    iBinaryOut = _iBinaryOut;
    dTime = _dTime;
    iType = TYPE_GAS;
}
double DustFracDotOutputParams::dValue(GravityParticle *p) {
    if (p->isGas())
        return p->dustFracDot();
    else
        return 0.0;
}
void DustFracDotOutputParams::setDValue(GravityParticle *p, double val) {
    if (p->isGas()) {
        p->dustFracDot() = val;
    }
}
Vector3D<double> DustFracDotOutputParams::vValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
int64_t DustFracDotOutputParams::iValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
void DustFracDotOutputParams::setIValue(GravityParticle *p, int64_t iValue) {
    CkAssert(0);
}

/**
 * @brief TstopOutputParams::TstopOutputParams is used for I/O of
 * a dust array (dustFracDot)
 * 
 * @param _fileName Base filename (excluding extension) for reading/writing
 * @param _iBinaryOut Flag for binary vs ascii files
 * @param _dTime Simulation time
 */
TstopOutputParams::TstopOutputParams(std::string _fileName, 
                                             int _iBinaryOut, double _dTime){
    // Set attribute values
    sTipsyExt = "tStop"; 
    sNChilExt = sTipsyExt;
    bFloat = 1;
    bVector = 0;
    fileName = _fileName; 
    iBinaryOut = _iBinaryOut;
    dTime = _dTime;
    iType = TYPE_GAS;
}
double TstopOutputParams::dValue(GravityParticle *p) {
    if (p->isGas())
        return p->tStop();
    else
        return 0.0;
}
void TstopOutputParams::setDValue(GravityParticle *p, double val) {
    if (p->isGas()) {
        p->tStop() = val;
    }
}
Vector3D<double> TstopOutputParams::vValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
int64_t TstopOutputParams::iValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
void TstopOutputParams::setIValue(GravityParticle *p, int64_t iValue) {
    CkAssert(0);
}
