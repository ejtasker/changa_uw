/*
 * Declares OutputParams (see InOutput.h) sub-classes used for I/O of dusty-gas
 * arrays.  In order to keep the dusty-gas implementation modular and separate,
 * and to minimize the amount of #ifdefs in the code, these classes are 
 * declared here, however it would be equivalent to place them in
 * InOutput.h
 * 
 * Author: Isaac Backus (University of Washington)
 * Created: 5/26/2016
 */

#ifndef DUSTOUTPUTPARAMS_H
#define DUSTOUTPUTPARAMS_H

#include "InOutput.h"

/**
 * @brief The DustFracOutputParams class is used for I/O of the dustFrac array
 */
class DustFracOutputParams : public OutputParams
{
    /* Declare virtual methods to override */
    virtual double dValue(GravityParticle *p);
    virtual void setDValue(GravityParticle *p, double val);
    virtual Vector3D<double> vValue(GravityParticle *p);
    virtual int64_t iValue(GravityParticle *p);
    virtual void setIValue(GravityParticle *p, int64_t iValue);
 public:
    DustFracOutputParams() {}
    DustFracOutputParams(std::string _fileName, int _iBinaryOut, double _dTime);
    // Make DustFracOutputParams PUPable
    PUPable_decl(DustFracOutputParams);
    DustFracOutputParams(CkMigrateMessage *m) {}
    virtual void pup(PUP::er &p) {
        OutputParams::pup(p);//Call base class
        }
};

/**
 * @brief The DustFracDotOutputParams class is used for I/O of a dust array
 */
class DustFracDotOutputParams : public OutputParams
{
    /* Declare virtual methods to override */
    virtual double dValue(GravityParticle *p);
    virtual void setDValue(GravityParticle *p, double val);
    virtual Vector3D<double> vValue(GravityParticle *p);
    virtual int64_t iValue(GravityParticle *p);
    virtual void setIValue(GravityParticle *p, int64_t iValue);
 public:
    DustFracDotOutputParams() {}
    DustFracDotOutputParams(std::string _fileName, int _iBinaryOut, double _dTime);
    // Make DustFracDotOutputParams PUPable
    PUPable_decl(DustFracDotOutputParams);
    DustFracDotOutputParams(CkMigrateMessage *m) {}
    virtual void pup(PUP::er &p) {
        OutputParams::pup(p);//Call base class
        }
};

/**
 * @brief The TstopOutputParams class is used for I/O of a dust array
 */
class TstopOutputParams : public OutputParams
{
    /* Declare virtual methods to override */
    virtual double dValue(GravityParticle *p);
    virtual void setDValue(GravityParticle *p, double val);
    virtual Vector3D<double> vValue(GravityParticle *p);
    virtual int64_t iValue(GravityParticle *p);
    virtual void setIValue(GravityParticle *p, int64_t iValue);
 public:
    TstopOutputParams() {}
    TstopOutputParams(std::string _fileName, int _iBinaryOut, double _dTime);
    // Make TstopOutputParams PUPable
    PUPable_decl(TstopOutputParams);
    TstopOutputParams(CkMigrateMessage *m) {}
    virtual void pup(PUP::er &p) {
        OutputParams::pup(p);//Call base class
        }
};

/**
 * @brief The DustVelOutputParams class is used for I/O of a dust array
 */
class DustVelOutputParams : public OutputParams
{
    /* Declare virtual methods to override */
    virtual double dValue(GravityParticle *p);
    virtual void setDValue(GravityParticle *p, double val);
    virtual Vector3D<double> vValue(GravityParticle *p);
    virtual int64_t iValue(GravityParticle *p);
    virtual void setIValue(GravityParticle *p, int64_t iValue);
 public:
    DustVelOutputParams() {}
    DustVelOutputParams(std::string _fileName, int _iBinaryOut, double _dTime);
    // Make DustVelOutputParams PUPable
    PUPable_decl(DustVelOutputParams);
    DustVelOutputParams(CkMigrateMessage *m) {}
    virtual void pup(PUP::er &p) {
        OutputParams::pup(p);//Call base class
        }
};

/**
 * @brief The OmegaKeplerOutputParams class is used for I/O of a dust array
 */
class OmegaKeplerOutputParams : public OutputParams
{
    /* Declare virtual methods to override */
    virtual double dValue(GravityParticle *p);
    virtual void setDValue(GravityParticle *p, double val);
    virtual Vector3D<double> vValue(GravityParticle *p);
    virtual int64_t iValue(GravityParticle *p);
    virtual void setIValue(GravityParticle *p, int64_t iValue);
 public:
    OmegaKeplerOutputParams() {}
    OmegaKeplerOutputParams(std::string _fileName, int _iBinaryOut, double _dTime);
    // Make OmegaKeplerOutputParams PUPable
    PUPable_decl(OmegaKeplerOutputParams);
    OmegaKeplerOutputParams(CkMigrateMessage *m) {}
    virtual void pup(PUP::er &p) {
        OutputParams::pup(p);//Call base class
        }
};

/**
 * @brief The DustGrainSizeOutputParams class is used for I/O of the dustGrainSize array
 */
class DustGrainSizeOutputParams : public OutputParams
{
    /* Declare virtual methods to override */
    virtual double dValue(GravityParticle *p);
    virtual void setDValue(GravityParticle *p, double val);
    virtual Vector3D<double> vValue(GravityParticle *p);
    virtual int64_t iValue(GravityParticle *p);
    virtual void setIValue(GravityParticle *p, int64_t iValue);
 public:
    DustGrainSizeOutputParams() {}
    DustGrainSizeOutputParams(std::string _fileName, int _iBinaryOut, double _dTime);
    // Make DustGrainSizeOutputParams PUPable
    PUPable_decl(DustGrainSizeOutputParams);
    DustGrainSizeOutputParams(CkMigrateMessage *m) {}
    virtual void pup(PUP::er &p) {
        OutputParams::pup(p);//Call base class
        }
};

/**
 * @brief The DustGrainSizeDotLocalOutputParams class is used for I/O of the dustGrainSizeDotLocal array
 */
class DustGrainSizeDotLocalOutputParams : public OutputParams
{
    /* Declare virtual methods to override */
    virtual double dValue(GravityParticle *p);
    virtual void setDValue(GravityParticle *p, double val);
    virtual Vector3D<double> vValue(GravityParticle *p);
    virtual int64_t iValue(GravityParticle *p);
    virtual void setIValue(GravityParticle *p, int64_t iValue);
 public:
    DustGrainSizeDotLocalOutputParams() {}
    DustGrainSizeDotLocalOutputParams(std::string _fileName, int _iBinaryOut, double _dTime);
    // Make DustGrainSizeDotLocalOutputParams PUPable
    PUPable_decl(DustGrainSizeDotLocalOutputParams);
    DustGrainSizeDotLocalOutputParams(CkMigrateMessage *m) {}
    virtual void pup(PUP::er &p) {
        OutputParams::pup(p);//Call base class
        }
};


/**
 * @brief The DustGrainSizeDotAdvectOutputParams class is used for I/O of the dustGrainSizeDotAdvect array
 */
class DustGrainSizeDotAdvectOutputParams : public OutputParams
{
    /* Declare virtual methods to override */
    virtual double dValue(GravityParticle *p);
    virtual void setDValue(GravityParticle *p, double val);
    virtual Vector3D<double> vValue(GravityParticle *p);
    virtual int64_t iValue(GravityParticle *p);
    virtual void setIValue(GravityParticle *p, int64_t iValue);
 public:
    DustGrainSizeDotAdvectOutputParams() {}
    DustGrainSizeDotAdvectOutputParams(std::string _fileName, int _iBinaryOut, double _dTime);
    // Make DustGrainSizeDotAdvectOutputParams PUPable
    PUPable_decl(DustGrainSizeDotAdvectOutputParams);
    DustGrainSizeDotAdvectOutputParams(CkMigrateMessage *m) {}
    virtual void pup(PUP::er &p) {
        OutputParams::pup(p);//Call base class
        }
};

/**
 * @brief The DustRelVelOutputParams class is used for I/O of the dustRelVel array
 */
class DustRelVelOutputParams : public OutputParams
{
    /* Declare virtual methods to override */
    virtual double dValue(GravityParticle *p);
    virtual void setDValue(GravityParticle *p, double val);
    virtual Vector3D<double> vValue(GravityParticle *p);
    virtual int64_t iValue(GravityParticle *p);
    virtual void setIValue(GravityParticle *p, int64_t iValue);
 public:
    DustRelVelOutputParams() {}
    DustRelVelOutputParams(std::string _fileName, int _iBinaryOut, double _dTime);
    // Make DustRelVelOutputParams PUPable
    PUPable_decl(DustRelVelOutputParams);
    DustRelVelOutputParams(CkMigrateMessage *m) {}
    virtual void pup(PUP::er &p) {
        OutputParams::pup(p);//Call base class
        }
};

/**
 * @brief The DustFracDotLossOutputParams class is used for I/O of a dust array
 */
class DustFracDotLossOutputParams : public OutputParams
{
    /* Declare virtual methods to override */
    virtual double dValue(GravityParticle *p);
    virtual void setDValue(GravityParticle *p, double val);
    virtual Vector3D<double> vValue(GravityParticle *p);
    virtual int64_t iValue(GravityParticle *p);
    virtual void setIValue(GravityParticle *p, int64_t iValue);
 public:
    DustFracDotLossOutputParams() {}
    DustFracDotLossOutputParams(std::string _fileName, int _iBinaryOut, double _dTime);
    // Make DustFracDotOutputParams PUPable
    PUPable_decl(DustFracDotLossOutputParams);
    DustFracDotLossOutputParams(CkMigrateMessage *m) {}
    virtual void pup(PUP::er &p) {
        OutputParams::pup(p);//Call base class
        }
};

/**
 * @brief The DustFracDotGainOutputParams class is used for I/O of a dust array
 */
class DustFracDotGainOutputParams : public OutputParams
{
    /* Declare virtual methods to override */
    virtual double dValue(GravityParticle *p);
    virtual void setDValue(GravityParticle *p, double val);
    virtual Vector3D<double> vValue(GravityParticle *p);
    virtual int64_t iValue(GravityParticle *p);
    virtual void setIValue(GravityParticle *p, int64_t iValue);
 public:
    DustFracDotGainOutputParams() {}
    DustFracDotGainOutputParams(std::string _fileName, int _iBinaryOut, double _dTime);
    // Make DustFracDotOutputParams PUPable
    PUPable_decl(DustFracDotGainOutputParams);
    DustFracDotGainOutputParams(CkMigrateMessage *m) {}
    virtual void pup(PUP::er &p) {
        OutputParams::pup(p);//Call base class
        }
};

template <typename T>
inline void writeArray(T outputparams, Main *main, parameters &param) {
    if (param.iBinaryOut) {
        main->outputBinary(outputparams, param.bParaWrite, 
                           CkCallbackResumeThread());
    } else {
        treeProxy[0].outputASCII(outputparams, param.bParaWrite,
                                 CkCallbackResumeThread());
    }
}
#endif // DUSTOUTPUTPARAMS_H
