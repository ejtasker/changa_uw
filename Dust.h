/*
 * Declares general methods for simulating "dust," i.e. solids particularly
 * in the case of protplanetary disks.
 * 
 * This is the general dust module file, for classes/functions used by all
 * dust implementations and by the dust sub-modules.
 * 
 * Author: Isaac Backus (University of Washington)
 * Created: 4/18/2016
 */
#ifndef DUST_H
#define DUST_H

#include "DustOneFluid.h"
#include "DustOneFluidGrains.h"

struct parameters;

/**
 * @brief The DustData class contains data to be managed by the DataManager.
 * 
 * Since this is not a particle-level class, it's not really in an inner-loop
 * in the code.  Therefore, it does not need to be highly optimized or 
 * stream-lined.  A few extra class members needed for only certain dust
 * sub-modules should be fine.
 */
class DustData : public PUP::able {
public:
    // Attributes
    Vector3D<double> starCenterOfMass;
    double starMass;
    // Methods
    DustData() {}
    void setStarCM(double dCenterOfMass[4]);
    // PUP stuff -- allows communication of DustData
    PUPable_decl(DustData);
    DustData(CkMigrateMessage *m) : PUP::able(m) {}
    void pup(PUP::er& p) {
        PUP::able::pup(p);
        p | starCenterOfMass;
        p | starMass;
    }
};

/*
 * DUST I/O
 */
/**
 * @brief The DustParam class contains parameters used for dusty gas
 * 
 * Since this is not a particle-level class, it's not really in an inner-loop
 * in the code.  Therefore, it does not need to be highly optimized or 
 * stream-lined.  A few extra class members needed for only certain dust
 * sub-modules should be fine.
 */
class DustParam {
public:
    double dDustGrainDensity;
    double dDustVfrag;
    double dDustAlphaSS;
    double dConstGrainSize;
    void AddParams(PRM prm);
    void CheckParams(PRM prm, parameters &param);
    inline void pup(PUP::er &p);
};
/* Make dust parameters shareable over the Charm++ framework */
inline void DustParam::pup(PUP::er &p) {
    p|dDustGrainDensity;
    p|dDustVfrag;
    p|dDustAlphaSS;
    p|dConstGrainSize;
}

#endif // DUST_H
