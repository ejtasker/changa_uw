/*
 * Defines empty functions to select turning the one fluid dust module off.  
 * 
 * For sufficiently high levels of optimization (O2 or O3), these empty 
 * functions should be automatically inlined out by the compiler.
 * 
 * Author: Isaac Backus (University of Washington)
 * Created: 10/12/2017
 */
#include "ParallelGravity.h"  // Things work best if this is included 1st
#include "DustOneFluid.h"

/* ------------------------------------
 * One fluid interface methods
 * ------------------------------------*/
namespace dustonefluid {

void updateAcc(GravityParticle *p, const Vector3D<double> &acc3d) {}
void scaleViscosity(GravityParticle *p, GravityParticle *q, double &visc){}
void localSPHterms(GravityParticle *p, parameters &param, double gamma){}
void openKick(GravityParticle *p, double dt) {}
void drift(GravityParticle *p, double dt) {}
void closeKick(GravityParticle *p, double dt) {}
double gasFracNorm(GravityParticle *p, double quantity) {
    return quantity;
}
void sinkDust(GravityParticle *p, GravityParticle *q) {}
/* dust Pressure Smooth */
void pressureSmooth::initSmoothParticle(GravityParticle *p){}
void pressureSmooth::initSmoothCache(GravityParticle *p) {}
void pressureSmooth::combSmoothCache(GravityParticle *p1, 
                                         ExternalSmoothParticle *p2) {}
void smoothDust(int activeRung, double dTime, parameters &param) {}

void init(parameters &param, int64_t nTotalParticles, double dTime);
void write(Main *main, parameters &param, std::string achFile, 
                   double dOutTime);

} // namespace dustonefluid
