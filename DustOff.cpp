/*
 * Defines empty functions to select turning the dustygas module off.  
 * 
 * For sufficiently high levels of optimization (O2 or O3), these empty 
 * functions should be automatically inlined out by the compiler.
 * 
 * Author: Isaac Backus (University of Washington)
 * Created: 4/29/2016
 */

#include "ParallelGravity.h"
#include "Dust.h"

/* ------------------------------------
 * General dust interface methods
 * ------------------------------------*/
/* AddParams */
void DustParam::AddParams(PRM prm){}
void DustParam::CheckParams(PRM prm, parameters &param){}

/* Main methods */
void Main::initDust() {}
void Main::writeDust(std::string achFile, double dOutTime) {}
