/*
 * Defines general methods for the one-fluid grain growth sub-module
 * 
 * By convention, everything declared in this sub-module (i.e. in the header
 * corresponding to this file) should be under this sub-module's namespace.
 * Everything declared outside this submodule and implemented here should be
 * outside this submodule's name-space.  This is both to protect names and to
 * make function calls more transparent when called from other parts of 
 * ChaNGa.
 * 
 * Also by convention, this module is turned off by compiling 
 * DustOneFluidGrainsOff.cpp (instead of this file) which contains empty
 * (or properly modified) function definitions for functions called by the
 * rest of the code.
 * 
 * Authors: Isaac Backus (University of Washington)
 *          Josef Rucska (McMaster University)
 * Created: 10/12/17
 */
#include "ParallelGravity.h"
#include "DustOneFluidGrains.h"
#include "DustOutputParams.h"
#include "float.h"
#include "Sph.h" // Needed only for arrayFileExists()
#include "Dust.h"

namespace onefluidgrains {

/* *********************************************
 * SmoothParams functions
 * *********************************************/
/**
 * @brief updateReceiver Updates grain size time-derivatives for particle
 * receiving dust from neighbor
 * @param receiver Particle receiving dust
 * @param donor Particle giving dust
 * @param dustFracDotVal Time-derivative factor
 */
void updateReceiver(GravityParticle *receiver, GravityParticle *donor, 
                    double dustFracDotVal) {
    receiver->dustFracDotGain() += donor->mass * dustFracDotVal;
    receiver->dustGrainSizeDotAdvect() += donor->mass * dustFracDotVal * 
        donor->dustGrainSizePred();
}

/**
 * @brief updateDonor Updates grain size time-derivatives for particle giving
 * dust to neighbor
 * @param receiver Particle receiving dust
 * @param donor Particle giving dust
 * @param dustFracDotVal Time-derivative factor
 */
void updateDonor(GravityParticle *receiver, GravityParticle *donor, 
                 double dustFracDotVal) {
    donor->dustFracDotLoss() -= receiver->mass * dustFracDotVal;
}

void initSmoothParticle(GravityParticle *p) {
    p->dustGrainSizeDotAdvect() = 0.0;
    p->dustFracDotLoss() = 0.0;
    p->dustFracDotGain() = 0.0;
}

void initSmoothCache(GravityParticle *p) {
    p->dustGrainSizeDotAdvect() = 0.0;
    p->dustFracDotLoss() = 0.0;
    p->dustFracDotGain() = 0.0;
}

void combSmoothCache(GravityParticle *p1, ExternalSmoothParticle *p2) {
    p1->dustGrainSizeDotAdvect() += p2->dustGrainSizeDotAdvect;
    p1->dustFracDotLoss() += p2->dustFracDotLoss;
    p1->dustFracDotGain() += p2->dustFracDotGain;
}

/**
 * @brief localGrainGrowth Calculates local grain growth time derivative
 * in the one-fluid grain model
 * @param p SPH particle to update
 * @param grainDensity Intrinisic grain density (in code units)
 * @param Vfrag Fragmentation velocity for dust grains (in code units)
 * @param alphaSS Constant for Shakura-Sunyaev viscosity in local grain growth
 * @param gamma Adiabatic index
 */
void localGrainGrowth(GravityParticle*p, DustParam& dustparam, double gamma) {
    double Stokes;
    // Calculate the Stokes parameter
    Stokes = sqrt(8./(M_PI * gamma)) * p->tStop() * p->omegaKepler() / 
                  (1 - p->dustFracPred());
    // Calculate sub-grid dust-dust relative velocity
    p->dustRelVel() = sqrt(2*sqrt(2)*ROSSBYNUMBER*dustparam.dDustAlphaSS) * 
        sqrt(Stokes)/(1+Stokes) * p->c();
    // Calculate Local dustGrainSize derivative based on collision 
    // between dust grains.
    if (p->dustRelVel() < dustparam.dDustVfrag) {
        p->dustGrainSizeDotLocal() = ((p->dustFracPred() * 
            (1 - p->dustFracPred()) * p->fDensity) / dustparam.dDustGrainDensity) * 
            p->dustRelVel();
    }
    else if (p->dustRelVel() >= dustparam.dDustVfrag) {
        p->dustGrainSizeDotLocal() = -((p->dustFracPred() * 
            (1 - p->dustFracPred()) * p->fDensity) / dustparam.dDustGrainDensity) * 
            p->dustRelVel();
    }
}

/* *********************************************
 * Kick-drift integration functions
 * *********************************************/
/**
 * @brief openKick Performs the kick "open" step of kick-drift integration for
 * the one fluid grain growth model.
 * @param p Pointer to the SPH particle to open the integration step for
 * @param dt Time step size (in code units)
 */
void openKick(GravityParticle *p, double dt) {
    p->dustGrainSizePred() = p->dustGrainSize();
    // Advection grain size update
    double dustFrac = p->dustFrac() + p->dustFracDotLoss() * dt;
    if (dustFrac < DUSTFRACLOSSEPS) dustFrac = DUSTFRACLOSSEPS;
    p->dustGrainSize() = p->dustGrainSize()*dustFrac 
            + p->dustGrainSizeDotAdvect()*dt;
    p->dustGrainSize() /= dustFrac + p->dustFracDotGain() * dt + DBL_MIN;
    // Local growth grain size update
    p->dustGrainSize() += p->dustGrainSizeDotLocal() * dt;
}

/**
 * @brief drift Performs the "drift" step of kick-drift integration for the
 * one fluid grain-growth model
 * @param p Pointer to the SPH particle to drift
 * @param dt Time step size (in code units)
 */
void drift(GravityParticle *p, double dt) {
    // Advection grain size update
    double dustFrac = p->dustFracPred() + p->dustFracDotLoss() * dt;
    if (dustFrac < DUSTFRACLOSSEPS) dustFrac = DUSTFRACLOSSEPS;
    p->dustGrainSizePred() = p->dustGrainSizePred() * dustFrac 
            + p->dustGrainSizeDotAdvect() * dt;
    p->dustGrainSizePred() /= dustFrac + p->dustFracDotGain() * dt + DBL_MIN;
    // Local growth grain size update
    p->dustGrainSizePred() += p->dustGrainSizeDotLocal() * dt;
}

/**
 * @brief closeKick Closes the kick (finishes the step) for kick-drift
 * integration for the one fluid grain growth model.
 * @param p Pointer to the SPH particle to close the integration step for
 * @param dt Time step size (in code units)
 */
void closeKick(GravityParticle *p, double dt) {
    // Advection grain size update
    double dustFrac = p->dustFrac() + p->dustFracDotLoss() * dt;
    if (dustFrac < DUSTFRACLOSSEPS) dustFrac = DUSTFRACLOSSEPS;
    p->dustGrainSize() = p->dustGrainSize() * dustFrac
            + p->dustGrainSizeDotAdvect() * dt;
    p->dustGrainSize() /= dustFrac +  p->dustFracDotGain() * dt + DBL_MIN;
    // Local growth grain size update
    p->dustGrainSize() += p->dustGrainSizeDotLocal() * dt;   
    p->dustGrainSizePred() = p->dustGrainSize();
}

/* *********************************************
 * Dust I/O and initialization
 * *********************************************/
void init(parameters &param, int64_t nTotalParticles, double dTime) {
    string dustfilename;
    // Read in dust grain size
    DustGrainSizeOutputParams pDustGrainSizeOut(param.achInFile, 0, dTime);
    dustfilename=std::string(param.achInFile) + "." + 
        pDustGrainSizeOut.sTipsyExt;
    if(arrayFileExists(dustfilename, nTotalParticles)){
        ckout << "\nLoading dust grain size " << dustfilename.c_str() << endl;
        treeProxy.readTipsyArray(pDustGrainSizeOut, CkCallbackResumeThread());
    }
    // Read in dust local grain size time derivative
    DustGrainSizeDotLocalOutputParams pDustGrainSizeDotLocalOut(param.achInFile,
                                                                0, dTime);
    dustfilename=std::string(param.achInFile) + "." + 
        pDustGrainSizeDotLocalOut.sTipsyExt;
    if(arrayFileExists(dustfilename, nTotalParticles)){
        ckout << "\nLoading local dust grain size time derivative " 
            << dustfilename.c_str() << endl;
        treeProxy.readTipsyArray(pDustGrainSizeDotLocalOut, 
            CkCallbackResumeThread());
    }   
    // Read in dust advected grain size time derivative
    DustGrainSizeDotAdvectOutputParams 
        pDustGrainSizeDotAdvectOut(param.achInFile, 0, dTime);
    dustfilename=std::string(param.achInFile) + "." + 
        pDustGrainSizeDotAdvectOut.sTipsyExt;
    if(arrayFileExists(dustfilename, nTotalParticles)){
        ckout << "\nLoading advected dust grain size time derivative " 
            << dustfilename.c_str() << endl;
        treeProxy.readTipsyArray(pDustGrainSizeDotAdvectOut, 
            CkCallbackResumeThread());
    }   
    // Read in gained dust fraction time derivative
    DustFracDotGainOutputParams 
        pDustFracDotGainOut(param.achInFile, 0, dTime);
    dustfilename=std::string(param.achInFile) + "." + 
        pDustFracDotGainOut.sTipsyExt;
    if(arrayFileExists(dustfilename, nTotalParticles)){
        ckout << "\nLoading gained dust fraction time derivative " 
            << dustfilename.c_str() << endl;
        treeProxy.readTipsyArray(pDustFracDotGainOut, 
            CkCallbackResumeThread());
    }
     // Read in lost dust fraction time derivative
    DustFracDotLossOutputParams 
        pDustFracDotLossOut(param.achInFile, 0, dTime);
    dustfilename=std::string(param.achInFile) + "." + 
        pDustFracDotLossOut.sTipsyExt;
    if(arrayFileExists(dustfilename, nTotalParticles)){
        ckout << "\nLoading lost dust fraction time derivative " 
            << dustfilename.c_str() << endl;
        treeProxy.readTipsyArray(pDustFracDotLossOut, 
            CkCallbackResumeThread());
    }
}

/**
 * @brief write writes arrays to disk associated with One-fluid dust grains
 * @param main Pointer to the simulation Main instance
 * @param param Reference to runtime parameters
 * @param achFile Out-file prefix
 * @param dOutTime Current simulation output time
 */
void write(Main *main, parameters &param, std::string achFile, double dOutTime) {
    // Initialize dust Param objects
    writeArray(OmegaKeplerOutputParams(achFile, param.iBinaryOut, dOutTime), 
               main, param);
    writeArray(DustGrainSizeOutputParams(achFile, param.iBinaryOut, dOutTime),
               main, param);
    writeArray(DustGrainSizeDotLocalOutputParams(achFile, param.iBinaryOut, 
                                                 dOutTime), main, param);
    writeArray(DustGrainSizeDotAdvectOutputParams(achFile, param.iBinaryOut, 
                                                  dOutTime), main, param);
    writeArray(DustRelVelOutputParams(achFile, param.iBinaryOut, dOutTime),
               main, param);
    writeArray(DustFracDotLossOutputParams(achFile, param.iBinaryOut, dOutTime),
               main, param);
    writeArray(DustFracDotGainOutputParams(achFile, param.iBinaryOut,dOutTime),
               main, param);
    writeArray(DustFracDotGainOutputParams(achFile, param.iBinaryOut, dOutTime),
               main, param);
}

} // namespace onefluidgrains

/* *********************************************
 * Outside the onefluidgrains namespace
 * *********************************************/
void DustData::setStarCM(double dCenterOfMass[4]) {
    for (int i=0; i<3; ++i) {
        starCenterOfMass[i] = dCenterOfMass[i];
    }
    starMass = dCenterOfMass[3];
}


/**
 * @brief TreePiece::updateDust Calculates keplerian angular velocity for 
 * particles.
 * @param activeRung current active rung
 */
void TreePiece::updateDust(int activeRung) {
    for (unsigned int i=1; i <= myNumParticles; ++i) {
        GravityParticle *p = &myParticles[i];
        if (p->isGas() && p->rung >= activeRung) {
            Vector3D<double> dx = p->position - dm->dustData->starCenterOfMass;
            // Use radius in x-y plane
            double r = sqrt(dx[0]*dx[0] + dx[1]*dx[1]);
            // Keplerian orbital angular velocity
            p->omegaKepler() = sqrt(dm->dustData->starMass/pow(r,3));
        }
    }
}

/* *********************************************
 * Array I/O class implementations
 * *********************************************/
/**
 * @brief OmegaKeplerOutputParams::OmegaKelperOutputParams is used for I/O of
 * a keplerian velocity array
 * 
 * @param _fileName Base filename (excluding extension) for reading/writing
 * @param _iBinaryOut Flag for binary vs ascii files
 * @param _dTime Simulation time
 */
OmegaKeplerOutputParams::OmegaKeplerOutputParams(std::string _fileName, 
                                             int _iBinaryOut, double _dTime){
    // Set attribute values
    sTipsyExt = "omegaKepler"; 
    sNChilExt = sTipsyExt;
    bFloat = 1;
    bVector = 0;
    fileName = _fileName; 
    iBinaryOut = _iBinaryOut;
    dTime = _dTime;
    iType = TYPE_GAS;
}
double OmegaKeplerOutputParams::dValue(GravityParticle *p) {
    if (p->isGas())
        return p->omegaKepler();
    else
        return 0.0;
}
void OmegaKeplerOutputParams::setDValue(GravityParticle *p, double val) {
    if (p->isGas()) {
        p->omegaKepler() = val;
    }
}
Vector3D<double> OmegaKeplerOutputParams::vValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
int64_t OmegaKeplerOutputParams::iValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
void OmegaKeplerOutputParams::setIValue(GravityParticle *p, int64_t iValue) {
    CkAssert(0);
}

/**
 * @brief DustGrainSizeOutputParams::DustGrainSizeOutputParams is used for I/O of the
 * dust grain size
 * 
 * @param _fileName Base filename (excluding extension) for reading/writing
 * @param _iBinaryOut Flag for binary vs ascii files
 * @param _dTime Simulation time
 */
DustGrainSizeOutputParams::DustGrainSizeOutputParams(std::string _fileName, 
                                           int _iBinaryOut, double _dTime){
    // Set attribute values
    sTipsyExt = "dustGrainSize";
    sNChilExt = sTipsyExt;
    bFloat = 1;
    bVector = 0;
    fileName = _fileName; 
    iBinaryOut = _iBinaryOut;
    dTime = _dTime;
    iType = TYPE_GAS | TYPE_STAR;
}
double DustGrainSizeOutputParams::dValue(GravityParticle *p) {
    if (p->isGas())
        return p->dustGrainSize();
    else
        return 0.0;
}
void DustGrainSizeOutputParams::setDValue(GravityParticle *p, double val) {
    if (p->isGas()) {
        p->dustGrainSize() = val;
        p->dustGrainSizePred() = val;
    }
}
Vector3D<double> DustGrainSizeOutputParams::vValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
int64_t DustGrainSizeOutputParams::iValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
void DustGrainSizeOutputParams::setIValue(GravityParticle *p, int64_t iValue) {
    CkAssert(0);
}

/**
 * @brief DustGrainSizeDotLocalOutputParams::DustGrainSizeDotLocalOutputParams is used for I/O of the
 * local dust grain size derivative
 * 
 * @param _fileName Base filename (excluding extension) for reading/writing
 * @param _iBinaryOut Flag for binary vs ascii files
 * @param _dTime Simulation time
 */
DustGrainSizeDotLocalOutputParams::DustGrainSizeDotLocalOutputParams(std::string _fileName, 
                                           int _iBinaryOut, double _dTime){
    // Set attribute values
    sTipsyExt = "dustGrainSizeDotLocal";
    sNChilExt = sTipsyExt;
    bFloat = 1;
    bVector = 0;
    fileName = _fileName; 
    iBinaryOut = _iBinaryOut;
    dTime = _dTime;
    iType = TYPE_GAS | TYPE_STAR;
}
double DustGrainSizeDotLocalOutputParams::dValue(GravityParticle *p) {
    if (p->isGas())
        return p->dustGrainSizeDotLocal();
    else
        return 0.0;
}
void DustGrainSizeDotLocalOutputParams::setDValue(GravityParticle *p, double val) {
    if (p->isGas()) {
        p->dustGrainSizeDotLocal() = val;
    }
}
Vector3D<double> DustGrainSizeDotLocalOutputParams::vValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
int64_t DustGrainSizeDotLocalOutputParams::iValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
void DustGrainSizeDotLocalOutputParams::setIValue(GravityParticle *p, int64_t iValue) {
    CkAssert(0);
}


/**
 * @brief DustGrainSizeDotAdvectOutputParams::DustGrainSizeDotAdvectOutputParams is used for I/O of the
 * dust grain size derivative due to advection
 * 
 * @param _fileName Base filename (excluding extension) for reading/writing
 * @param _iBinaryOut Flag for binary vs ascii files
 * @param _dTime Simulation time
 */
DustGrainSizeDotAdvectOutputParams::DustGrainSizeDotAdvectOutputParams(std::string _fileName, 
                                           int _iBinaryOut, double _dTime){
    // Set attribute values
    sTipsyExt = "dustGrainSizeDotAdvect";
    sNChilExt = sTipsyExt;
    bFloat = 1;
    bVector = 0;
    fileName = _fileName; 
    iBinaryOut = _iBinaryOut;
    dTime = _dTime;
    iType = TYPE_GAS | TYPE_STAR;
}
double DustGrainSizeDotAdvectOutputParams::dValue(GravityParticle *p) {
    if (p->isGas())
        return p->dustGrainSizeDotAdvect();
    else
        return 0.0;
}
void DustGrainSizeDotAdvectOutputParams::setDValue(GravityParticle *p, double val) {
    if (p->isGas()) {
        p->dustGrainSizeDotAdvect() = val;
    }
}
Vector3D<double> DustGrainSizeDotAdvectOutputParams::vValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
int64_t DustGrainSizeDotAdvectOutputParams::iValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
void DustGrainSizeDotAdvectOutputParams::setIValue(GravityParticle *p, int64_t iValue) {
    CkAssert(0);
}


/**
 * @brief DustRelVelOutputParams::DustRelVelOutputParams is used for I/O of the
 * subgrid dust-gas relative velocity
 * 
 * @param _fileName Base filename (excluding extension) for reading/writing
 * @param _iBinaryOut Flag for binary vs ascii files
 * @param _dTime Simulation time
 */
DustRelVelOutputParams::DustRelVelOutputParams(std::string _fileName, 
                                           int _iBinaryOut, double _dTime){
    // Set attribute values
    sTipsyExt = "dustRelVel";
    sNChilExt = sTipsyExt;
    bFloat = 1;
    bVector = 0;
    fileName = _fileName; 
    iBinaryOut = _iBinaryOut;
    dTime = _dTime;
    iType = TYPE_GAS | TYPE_STAR;
}
double DustRelVelOutputParams::dValue(GravityParticle *p) {
    if (p->isGas())
        return p->dustRelVel();
    else
        return 0.0;
}
void DustRelVelOutputParams::setDValue(GravityParticle *p, double val) {
    if (p->isGas()) {
        p->dustRelVel() = val;
    }
}
Vector3D<double> DustRelVelOutputParams::vValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
int64_t DustRelVelOutputParams::iValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
void DustRelVelOutputParams::setIValue(GravityParticle *p, int64_t iValue) {
    CkAssert(0);
}


/**
 * @brief DustFracDotLossOutputParams::DustFracDotLossOutputParams is used for 
 * I/O of a dust array (dustFracDotLoss)
 * 
 * @param _fileName Base filename (excluding extension) for reading/writing
 * @param _iBinaryOut Flag for binary vs ascii files
 * @param _dTime Simulation time
 */
DustFracDotLossOutputParams::DustFracDotLossOutputParams(std::string _fileName, 
                                             int _iBinaryOut, double _dTime){
    // Set attribute values
    sTipsyExt = "dustFracDotLoss"; 
    sNChilExt = sTipsyExt;
    bFloat = 1;
    bVector = 0;
    fileName = _fileName; 
    iBinaryOut = _iBinaryOut;
    dTime = _dTime;
    iType = TYPE_GAS;
}
double DustFracDotLossOutputParams::dValue(GravityParticle *p) {
    if (p->isGas())
        return p->dustFracDotLoss();
    else
        return 0.0;
}
void DustFracDotLossOutputParams::setDValue(GravityParticle *p, double val) {
    if (p->isGas()) {
        p->dustFracDotLoss() = val;
    }
}
Vector3D<double> DustFracDotLossOutputParams::vValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
int64_t DustFracDotLossOutputParams::iValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
void DustFracDotLossOutputParams::setIValue(GravityParticle *p, int64_t iValue) {
    CkAssert(0);
}

/**
 * @brief DustFracDotGainOutputParams::DustFracDotGainOutputParams is used for 
 * I/O of a dust array (dustFracDotGain)
 * 
 * @param _fileName Base filename (excluding extension) for reading/writing
 * @param _iBinaryOut Flag for binary vs ascii files
 * @param _dTime Simulation time
 */
DustFracDotGainOutputParams::DustFracDotGainOutputParams(std::string _fileName, 
                                             int _iBinaryOut, double _dTime){
    // Set attribute values
    sTipsyExt = "dustFracDotGain"; 
    sNChilExt = sTipsyExt;
    bFloat = 1;
    bVector = 0;
    fileName = _fileName; 
    iBinaryOut = _iBinaryOut;
    dTime = _dTime;
    iType = TYPE_GAS;
}
double DustFracDotGainOutputParams::dValue(GravityParticle *p) {
    if (p->isGas())
        return p->dustFracDotGain();
    else
        return 0.0;
}
void DustFracDotGainOutputParams::setDValue(GravityParticle *p, double val) {
    if (p->isGas()) {
        p->dustFracDotGain() = val;
    }
}
Vector3D<double> DustFracDotGainOutputParams::vValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
int64_t DustFracDotGainOutputParams::iValue(GravityParticle *p) {
    CkAssert(0);
    return 0.0;
}
void DustFracDotGainOutputParams::setIValue(GravityParticle *p, int64_t iValue) {
    CkAssert(0);
}
