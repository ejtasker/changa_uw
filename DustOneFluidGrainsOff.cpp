/*
 * Defines empty functions to select turning the one fluid grain growth
 * model.
 * 
 * Only interface routines (i.e. used by other parts of ChaNGa) need be
 * defined here.
 * 
 * For sufficiently high levels of optimization (O2 or O3), these empty 
 * functions should be automatically inlined out by the compiler.
 * 
 * Author: Isaac Backus (University of Washington)
 * Created: 10/12/2017
 */
#include "ParallelGravity.h"
#include "DustOneFluidGrains.h"
#include "Dust.h"

void TreePiece::updateDust(int activeRung) {}
void DustData::setStarCM(double dCenterOfMass[4]) {}

namespace onefluidgrains {
void updateReceiver(GravityParticle *receiver, GravityParticle *donor, 
                    double dustFracDotVal){}
void updateDonor(GravityParticle *receiver, GravityParticle *donor, 
                 double dustFracDotVal){}
void initSmoothParticle(GravityParticle *p){}
void initSmoothCache(GravityParticle *p){}
void combSmoothCache(GravityParticle *p1, ExternalSmoothParticle *p2){}
void init(parameters &param, int64_t nTotalParticles, double dTime){}
void localGrainGrowth(GravityParticle*p, DustParam& dustparam, double gamma){}
void openKick(GravityParticle *p, double dt){}
void drift(GravityParticle *p, double dt){}
void closeKick(GravityParticle *p, double dt){}
void write(Main *main, parameters &param, std::string achFile, double dOutTime){}

} // namespace onefluidgrains
