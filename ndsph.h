/*
 * This file defines inline functions and macros necessary for using SPH in
 * 1 and 2 dimensions. 
 */
#ifndef NDSPH_H
#define NDSPH_H

/* Compile flag checks*/
#if WENDLAND == 1
    #if NDSPH == 1
        #error Wendland kernel not implemented for 1D SPH
    #endif //NDSPH==1
#endif //WENDLAND

/*
 * Define correction scaling for Wendland self contribution to the density
 * estimate.  For ndim = 3 we nSmooth by 100.  For ndim = 2, we scale by 
 * 100^2/3 = 21.5.
 */
#ifdef NDSPH
    #if NDSPH == 2
        // Divide by nSmooth = 21.5
        const double wendlandCorr = 0.0464158883;
    #elif NDSPH == 3
        // Divide by nSmooth = 100 as in Dehnen & Aly 2012
        const double wendlandCorr = 0.01;
    #else
        // Required to compile other NDSPH values, even if wendland is not used
        const double wendlandCorr = 0.01;
    #endif
#else
    // Divide by nSmooth = 100 as in Dehnen & Aly 2012
    const double wendlandCorr = 0.01;
#endif

 /**
 * @brief kernelDimNorm Changes the normalization of the SPH Kernel for
 * SPH in dimensions other than 3.
 * 
 * For the M4 (cubic) kernel of Monaghan 1992, the kernels and the kernel
 * gradients are just scaled by the following factors:
 *      ndim = 1: (2pi/3) h^2
 *      ndim = 2: (10/7) h
 *      ndim = 3: 1
 * @param &val Value to normalize
 * @param ih2 Inverse of smoothing length squared
 */
inline void m4KernelNorm(double &val, double ih2) {
#ifdef NDSPH
    #if NDSPH == 1
        val*= (2./3.) * M_PI / ih2;
    #elif NDSPH == 2
        val *= (10./7.) / sqrt(ih2);
    #elif NDSPH == 3
        ;
    #else
        #error Unknown value for NDSPH
    #endif
#endif
}

/**
 * @brief m6KernelNorm changes the normalization of the M6 quintic-spline SPH
 * kernel for SPH in dimensions other than 3
 * 
 * For the M6 (quintic) kernel (see e.g. Dehnen & Aly 2012) the kernels and 
 * gradients are scaled by the following factors:
 *      ndim = 1: (pi/9) H^2
 *      ndim = 2: (280/478) H
 *      ndim = 3: 1
 * By convention in ChaNGa, H = 2*h (twice the smoothing length)
 * @param val value to normalize
 * @param ih2 Inverse of smoothing length squared
 */
inline void m6KernelNorm(double &val, double ih2) {
#ifdef NDSPH
    #if NDSPH == 1
        val*= 4. * M_PI / (9. * ih2);
    #elif NDSPH == 2
        val *= (280./239.) / sqrt(ih2);
    #elif NDSPH == 3
        ;
    #else
        #error Unknown value for NDSPH
    #endif
#endif
}

/**
 * @brief wendlandKernelNorm Changes the normalization of the SPH Wendland
 * kernel for SPH in dimensions other than 3.  Currently, only ND=2,3 are
 * implemented.
 * 
 * @param val
 * @param ih2
 */
inline void wendlandKernelNorm(double &val, double ih2) {
#ifdef NDSPH
    #if NDSPH == 1
        CkError("Wendland kernel not implemented for 1D SPH");
    #elif NDSPH == 2
        val *= (64./55.) / sqrt(ih2);
    #elif NDSPH == 3
        ;
    #else
        #error Unknown value for NDSPH
    #endif
#endif
}

/**
* @brief zeroUnusedDim zeros a particle's velocity and acceleration along the
* unused dimensions when using n-dimensional sph.
* 
* The used dimensions for the different NDSPH are:
*   1: z
*   2: y, z
*   3: x, y, z
* @param p
*/
inline void zeroUnusedDim(GravityParticle *p) {
    #ifdef NDSPH
    for (int i = 0; i < 3-NDSPH; i++) {
        p->treeAcceleration[i] = 0.0;
        p->velocity[i] = 0.0;
    }
    #endif
}

#endif // NDSPH_H
